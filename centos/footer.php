<div class="hr">
  <div class="hr__centos-color-0"></div>
  <div class="hr__centos-color-1"></div>
  <div class="hr__centos-color-2"></div>
  <div class="hr__centos-color-3"></div>
</div>
<footer class="footer">
	<div class="container">
		<div class="row">
			<section class="links">
				<h6><i class="fas fa-info-circle"></i> About</h6>
				<ul>
					<li><a href="<?php the_centos_url(); ?>/about">About CentOS</a></li>
					<li><a href="<?php the_centos_url('wiki'); ?>/FAQ">Frequently Asked Questions (FAQs)</a></li>
					<li><a href="<?php the_centos_url('wiki'); ?>/SpecialInterestGroups">Special Interest Groups (SIGs)</a></li>
					<li><a href="<?php the_centos_url(); ?>/variants">CentOS Variants</a></li>
					<li><a href="<?php the_centos_url(); ?>/about/governance">Governance</a></li>
					<li><a href="<?php the_centos_url(); ?>/code-of-conduct">Code of Conduct</a></li>
				</ul>
			</section>
			<section class="links">
				<h6><i class="fas fa-users"></i> Community</h6>
				<ul>
					<li><a href="<?php the_centos_url('wiki'); ?>/Contribute">Contribute</a></li>
					<li><a href="<?php the_centos_url(); ?>/forums/">Forums</a></li>
					<li><a href="<?php the_centos_url('wiki'); ?>/GettingHelp/ListInfo">Mailing Lists</a></li>
					<li><a href="<?php the_centos_url('wiki'); ?>/irc">IRC</a></li>
					<li><a href="<?php the_centos_url(); ?>/community/calendar/">Calendar &amp; IRC Meeting List</a></li>
					<li><a href="<?php the_centos_url('planet'); ?>/">Planet</a></li>
					<li><a href="<?php the_centos_url('wiki'); ?>/ReportBugs">Submit Bug</a></li>
				</ul>
			</section>
			<section class="project">
				<h4>The CentOS Project</h4>
				<p class="lead">Community-driven free software effort focused around the goal of providing a rich base platform for open source communities to build upon.</p>
				<div class="lead social">
					<a href="https://www.facebook.com/groups/centosproject/"><i class="fab fa-facebook-f"></i></a>
					<a href="https://twitter.com/centosproject"><i class="fab fa-twitter"></i></a>
					<a href="https://youtube.com/TheCentOSProject"><i class="fab fa-youtube"></i></a>
					<a href="https://www.linkedin.com/groups/22405"><i class="fab fa-linkedin"></i></a>
					<a href="https://www.reddit.com/r/CentOS/"><i class="fab fa-reddit"></i></a>
				</div>
			</section>
		</div>
		<div class="row">
			<section class="copyright">
				<p>Copyright &copy; 2021 The CentOS Project  | <a href="<?php the_centos_url(); ?>/legal">Legal</a> | <a href="<?php the_centos_url(); ?>/legal/privacy">Privacy</a></p>
			</section>
		</div>
	</div>
</footer>
</body>
</html>
