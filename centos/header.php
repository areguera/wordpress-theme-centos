<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head(); ?>
	</head>
	<body>
		<?php if ( is_user_logged_in() ) : ?>
		<nav class="navbar fixed-top navbar-expand-lg navbar-dark mt-4">
			<?php else : ?>
			<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
				<?php endif ?>
				<div class="container">
					<a class="navbar-brand" href="<?php the_centos_url(); ?>/"><img src="<?php the_centos_url(); ?>/assets/img/logo.png" height="32" alt="The CentOS Project"> <span class="manifestation"></span></a>
					<button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
					<div class="collapse navbar-collapse" id="navcol-1">
						<ul class="nav navbar-nav ml-auto">
							<li class="nav-item" role="presentation"><a class="nav-link" href="<?php the_centos_url(); ?>/download"><i class="fas fa-download"></i> Download</a></li>
							<li class="nav-item dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-info-circle"></i> About</a>
								<div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/about">About CentOS</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/FAQ">Frequently Asked Questions (FAQs)</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/SpecialInterestGroups">Special Interest Groups (SIGs)</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/variants">CentOS Variants</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/about/governance">Governance</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/code-of-conduct">Code of Conduct</a>
								</div>
							</li>
							<li class="nav-item dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-users"></i> Community</a>
								<div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/Contribute">Contribute</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/forums/">Forums</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/GettingHelp/ListInfo">Mailing Lists</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/irc">IRC</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/community/calendar/">Calendar &amp; IRC Meeting List</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('planet'); ?>/">Planet</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/ReportBugs">Submit Bug</a>
								</div>
							</li>
							<li class="nav-item dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-book"></i> Documentation</a>
								<div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php the_centos_url('wiki'); ?>/">Wiki</a>
									<a class="dropdown-item" role="presentation" href="https://docs.centos.org/">Manuals</a>
									<a class="dropdown-item" role="presentation" href="<?php the_centos_url(); ?>/keys">GPG Key Info</a>
								</div>
							</li>
							<li class="nav-item" role="presentation"><a class="nav-link" href="<?php the_centos_url('wiki'); ?>/Documentation?action=show&amp;redirect=GettingHelp"><i class="fas fa-life-ring"></i> Help</a></li>
							<li class="nav-item" role="presentation"><a class="nav-link" href="<?php the_centos_url(); ?>/search"><i class="fas fa-search"></i> Search</a></li>
						</ul>
					</div>
				</div>
			</nav>
			<?php wp_admin_bar_render(); ?>
