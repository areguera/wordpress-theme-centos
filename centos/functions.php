<?php
/**
 * CentOS functions and definitions
 */


/**
 * CentOS URLs
 *
 * This section controls the URLs we use to retrieve stylesheets, images, and
 * js files. Consider this useful when you are using different environments,
 * one for production and one for development, and need to change the URLs from
 * one to another for testing purposes.
 */
function the_centos_url( $site_id="www" ) {
	echo get_centos_url( $site_id );
}
function get_centos_url( $site_id="www" ) {
	if ( $site_id == "blog" ) {
		// The CentOS Blog
		return get_home_url();
	} elseif ( $site_id == "wiki" ) {
		// The CentOS Wiki
		return "https://wiki.centos.org";
	} elseif ( $site_id == "planet" ) {
		// The CentOS Planet
		return "https://planet.centos.org";
	} else {
		// The single source of truth for static content like stylesheets,
		// images and js files.
		return "https://centos.areguera.net";
	}
}

/**
 * CentOS Style
 *
 * The stylesheet used by wordpress site is not in the them directory structure
 * but in a remote location that we use as single point of change.
 */
function centos_scripts() {
	wp_enqueue_style('centos-blog', get_centos_url() . '/assets/css/centos-blog.bootstrap.min.css');
	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_script('centos-jquery', get_centos_url() . '/assets/js/jquery.min.js');
	wp_enqueue_script('centos-bootstrap', get_centos_url() .'/assets/js/bootstrap.min.js');
}
add_action('wp_enqueue_scripts', 'centos_scripts');

/**
 * CentOS Favicon
 */
function centos_favicon(){
    echo '<link rel="shortcut icon" href="'. get_centos_url() .'/assets/img/favicon.png" />';
}
add_action('wp_head', 'centos_favicon');

/**
 * CentOS Sidebar
 */
function centos_widgets_init() {
    register_sidebar( array (
        'name'          => __('Primary Sidebar', 'centos'),
        'id'            => 'sidebar-1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="font-weight-bold">',
        'after_title'   => '</div>',
   ));
}
add_action('widgets_init', 'centos_widgets_init');

/**
 * CentOS Navbar and Wordpress administration bar integration
 *
 * By default the admin bar consumes 32px height, always. This adds an extra
 * space on top of the navigation bar which affects the CentOS navigation bar
 * expected presentation. Here (and in header.php) we remove that space and fix
 * the CentOS navigation bar presentation to deal with Wordpress admin bar
 * based on whether the user is logged in or not.
 */
if (is_user_logged_in()) {
	add_filter('show_admin_bar' , '__return_true');
} else {
	add_filter('show_admin_bar' , '__return_false');
}

/**
 * CentOS Title
 */

function centos_title() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'centos_title' );
?>
