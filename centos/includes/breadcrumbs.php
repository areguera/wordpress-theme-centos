<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="<?php the_centos_url(); ?>/">Home</a></li>
	<li class="breadcrumb-item"><a href="<?php the_centos_url("blog"); ?>">Blog</a></li>
	<?php if ( is_search() ) : ?>
	<li class="breadcrumb-item">Search results for "<?php echo get_search_query(); ?>"</li>
	<?php elseif ( is_category() ) : ?>
	<li class="breadcrumb-item">Categories "<?php single_cat_title(); ?>"</li>
	<?php elseif ( is_archive() ) : ?>
	<li class="breadcrumb-item">Archives "<?php the_archive_title(); ?>"</li>
	<?php elseif ( is_single() ) : ?>
	<li class="breadcrumb-item"><?php the_title(); ?></li>
	<?php endif ?>
</ol>
