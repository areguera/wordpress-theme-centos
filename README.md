# wordpress-theme-centos

Wordpress theme for CentOS Blog.

# Screenshot

![screenshot](centos/screenshot.png)

# Installation

Manual installation:

1. Upload the `centos` folder to the `/wp-content/themes/` directory

Installation using "Add New Theme"

1. From your Admin UI (Dashboard), use the menu to select Themes -> Add New
2. Search for `centos`
3. Click the 'Install' button to open the theme's repository listing
4. Click the 'Install' button

# Activation and Use

Activate the Theme through the 'Themes' menu in WordPress

# License

MIT License.
